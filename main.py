# TODO the goal of this project is to
# Utilize learn the residual policy

# Moreover, compared to rudder.
# We know the alternation of the policy may stress the extra rewards or return.
# We allocate the rewards to those alternation.

# What RUDDER has achieved?
# Use a LSTM to predict many rewards ?
# Predict the delta Q directly ?
# r_t = Q(s_t+1) - Q(s_t)

# What ResRL will achieve ?
# We alter the policy to learn extra values ?
# Normally zero.
# Key alternations -> values increment.
# Do no penalization to those .

# Take Mujoco Locomotion problem.
# The output is a Gaussian  Distribution.

# How to modify the distribution.
# Use another distribution to add or substract.

# Basic elements.
# 1. Values.
# 2. Actor -> Policies\

# Structure
# Top: PPO - Learn from the start
# Bottom: Slow Learning Rate or Alternative Training

# How to deal with the top policy when base policy changed.
# Continuous
# Base Policy:  mean + sigma
# Base Policy Copy.

# Top Policy: mean_shift + log(sigma_scale)

# Discrete
# Base Policy: probability distribution
# Base Policy Copy.

# Top Policy: probability modification